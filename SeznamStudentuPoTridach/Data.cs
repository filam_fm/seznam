﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeznamStudentuPoTridach
{
    public class Data : ViewBaseModel
    {
        public ObservableCollection<Student> Studenti;
        private Student _selectedStudent;
        public Student SelectedStudent
        {
            get { return _selectedStudent; }
            set { _selectedStudent = value; OnPropertyChange("SelectedStudent"); }
        }
        public Data()
        {
            Studenti = new ObservableCollection<Student>()
            {
                new Student("Jan","Novák"),
                new Student("Michal", "Roháček")
            };
            SelectedStudent = Studenti.First();
        }
    }
}
