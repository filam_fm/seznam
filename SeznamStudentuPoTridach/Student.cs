﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeznamStudentuPoTridach
{
    public class Student : ViewBaseModel
    {
        private string _jmeno;
        public string Jmeno
        {
            get { return _jmeno; }
            set { _jmeno = value; OnPropertyChange("Jmeno"); }
        }
        private string _prijmeni;
        public string Prijmeni
        {
            get { return _prijmeni; }
            set { _prijmeni = value; OnPropertyChange("Prijmeni"); }
        }
        public Student(string jmeno, string prijmeni)
        {
            Jmeno = jmeno;
            Prijmeni = prijmeni;
        }
        public override string ToString()
        {
            return $"{Jmeno} {Prijmeni}";
        }
    }
}
